# files
so there are 3 files in this project: template_best.py and template_latest.py
* template_best.py is the solution that I think scored 1.58 but it has some stuff that might make the code harder to read
* template_latest.py is the final submission that I made to code judge I think it should also score 1.58, but I can't know for sure. It should at least be very close, but this file is much easier to read and I put some comments here explaining what the thought was behind soem code segments.
* generate_log.py was one of the things I used to test the speed of my code (you can comment out the generate() function so it doesn't generate a new logfile each time))