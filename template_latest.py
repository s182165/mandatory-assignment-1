from collections import defaultdict
from datetime import datetime as dt, timezone, timedelta
import bisect

month_dict = {
    "Jan" : 1,
    "Feb" : 2,
    "Mar" : 3,
    "Apr" : 4,
    "May" : 5,
    "Jun" : 6,
    "Jul" : 7,
    "Aug" : 8,
    "Sep" : 9,
    "Oct" : 10,
    "Nov" : 11,
    "Dec" : 12,

}
# calculating this value more than once takes time, easier to just have it saved
zero_time_offset = timezone(timedelta(hours=0, minutes=0))

def parse_time(time_str):
    # parsing this by hand like this is way faster than using strptime (around 1 second in competititon)
    day = int(time_str[1:3])
    month = month_dict[time_str[4:7]]
    year = int(time_str[8:12])
    hr = int(time_str[13:15])
    mn = int(time_str[16:18])
    sc = int(time_str[19:21])
    return dt(year, month, day, hr, mn, sc, 0, zero_time_offset)


class LogAnalyzer:
    # This should parse the log file named filename, store relevant data in the object,
    # and finally return the number of (non-empty) log lines in the file.
    # See https://en.wikipedia.org/wiki/Common_Log_Format for the format.
    def __init__(self):
        self.ips = defaultdict(int)
        self.status_codes = defaultdict(int)
        self.times = []
        self.query_lookup_single = None
        self.query_lookup_multi = None
        self.query_cache = {}
        self.sorted_resources = None
        self.unique_resources = None
        self.avg_bytes = 0
        self.max_ip = None

    def parse(self, filename):
        n_bytes = 0
        resources = []
        unique_resource = set()
        with open(filename, "r") as file:
            for line in file:
                parts = line.split(" ")
                ip = parts[0]
                self.times.append(parse_time(parts[3]))
                resource = parts[6]
                n_bytes += int(parts[9])
                resources.append(resource)
                unique_resource.add(resource)
                self.status_codes[int(parts[8])] += 1
                self.ips[ip] += 1

        # calcualte the maximum ip once and save the result to return it
        self.max_ip = ip
        for i in self.ips.keys():
            if self.ips[i] > self.ips[self.max_ip]:
                self.max_ip = i

        self.sorted_resources = sorted(resources)
        self.unique_resources = sorted(unique_resource)
        self.times = sorted(self.times)

        # calculate the average byte size once to save it
        self.avg_bytes = n_bytes/len(resources)

        # prepare dictionaries for use in Query functions
        self.query_lookup_multi = defaultdict(set)
        self.query_lookup_single = defaultdict(list)
        for i, query in enumerate(self.sorted_resources):
            params = query.split("?")[1].split("&")
            for param in params:
                self.query_lookup_multi[param].add(i)
                self.query_lookup_single[param].append(query)

        return len(resources)

    # The average of object sizes of all requests
    def averageObjectSize(self):
        return self.avg_bytes

    # The most frequent ip address among the log lines.
    # (if multiple exists, just return any of them)
    def mostFrequentIp(self):
        # lazy evaluation
        return self.max_ip

    # Should return a list of all resources requested in sorted order
    def resources(self):
        return self.sorted_resources

    # Should return a list of all distinct resources requested in sorted order
    def distinctResources(self):
        return self.unique_resources

    # Should return the number of requests that gave the given HTTP status code.
    def numberOfStatusCode(self, statuscode):
        return self.status_codes[statuscode]

    # Should return the number of requests made in the given time range (inclusive start/end).
    # start and end will be given as datetime objects (described on https://docs.python.org/2/library/datetime.html)
    def countRequestsInTimeRange(self, start, end):
        # since times are sorted we do binary search to find the correct places for the start and end
        lo = bisect.bisect_left(self.times, start)
        hi = bisect.bisect_right(self.times, end, lo=lo)
        return hi - lo


    # This function should return a list of resources that fulfill a given criteria in sorted order.
    # To solve this, you must parse the query string part of the resources in the log file.
    # For example the resource /showcars?year=2010&color=red has two query string parameters,
    # one with the name year and value 2010 and one with name color and value red.
    # See more details on https://en.wikipedia.org/wiki/Query_string.
    # Given a name and value of such a query string parameter, return the list of all
    # resources that match (again in sorted order after resource name).
    def resourcesWithQueryParam(self, name, value):
        # we already calculated this so we can return it immediately
        return self.query_lookup_single[name+"="+value]


    # This is like the previous exercise, except you are now given a list of criterias
    # instead of a single criteria. This function should return a list of all the resources
    # that match _all_ the criterias in the list (again in sorted order after resource name).
    # The criterias are given as a list of tuples where the first element in the tuple is the
    # name and the second the value. For instance queryParams could be
    # [("year", "2010"), ("color", "red")].
    def resourcesWithAllQueryParams(self, queryParams):
        if len(queryParams) == 1:
            name, value = queryParams[0]
            return self.query_lookup_single[name+"="+value]

        sets = []
        query_strings = []

        # for each query parameter we look up which set of resources (as indexes into self.sorted_resources)
        # has those parameters, we also put the strings together in a list
        for name, value in queryParams:
            query = name+"="+value
            query_strings.append(query)
            sets.append(self.query_lookup_multi[query])

        # by sorting the list of query strings we can be sure that if we have seen this set of query strings before
        # that we can look them up in our query cache and return without recalculating
        query_strings.sort()
        cache_key = ''.join(query_strings)
        if cache_key in self.query_cache.keys():
            return self.query_cache[cache_key]

        # take teh sets of resources that we have found and calculate the intersection of these sets
        # the resulting set should only contain the indices of the relevant resources
        final_set = set.intersection(*sets)

        # sorting final_set is cheaper than sorting the result here because final set has integers while the
        # result has strings
        res = [self.sorted_resources[i] for i in sorted(final_set)]

        # store the result in case we see it again
        self.query_cache[cache_key] = res
        return res
