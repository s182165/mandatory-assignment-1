import random
from template_latest import LogAnalyzer
import time

prefix = '71.63.58.36 - - [03/Sep/2018:10:13:33 +0000] "POST '
suffix = ' HTTP/1.0" 501 70645'

possible_locations = ["/cars/count?", "/bars/count?", "/bars/chocolate?", "/cars/search?"]

possible_name_values = {
    "color": ["yellow", "red", "green", "orange", "purple", "blue"],
    "year": [str(i) for i in range(2000, 2016)],
    "smorf": ["aaaa", "bbbb", "cccc", "dddd", "eeee", "ffff", "gggg", "hhhh", "iiii", "jjjj"],
    "number": [str(i) for i in range(100)]
}
possible_keys = list(possible_name_values.keys())


def generate():
    strings = []
    for i in range(100000):
        n_params = random.randint(1, 4)
        random.shuffle(possible_keys)
        keys = possible_keys[:n_params]
        location = possible_locations[random.randint(0, 3)]
        params = []
        for i in keys:
            values = possible_name_values[i]
            params.append(i + "=" + values[random.randint(0, len(values) - 1)])
        q = "&".join(params)
        strings.append(prefix + location + q + suffix + "\n")
    with open("webserver_rand.log", "w") as file:
        for i in strings:
            file.write(i)


def test_analyzer(analyzer):
    query_params = []
    for i in range(100000):
        n_params = 2
        random.shuffle(possible_keys)
        keys = possible_keys[:n_params]
        params = []
        for i in keys:
            values = possible_name_values[i]
            params.append((i, values[random.randint(0, len(values) - 1)]))
        query_params.append(params)

    start = time.time()

    for query in query_params:
        res_new = analyzer.resourcesWithAllQueryParams(query)
    end = time.time()
    print(end - start)


generate()
analyzer = LogAnalyzer()
analyzer.parse("webserver_rand.log")

test_analyzer(analyzer)